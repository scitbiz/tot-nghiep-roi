package com.example.mac.phonenumber;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.HashMap;
import java.util.Map;

public class DbHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "phones.db";
    public static final int DATABASE_VERSION = 1;

    public static final String TABLE_PRODUCER = "producer";
    public static final String TABLE_PHONE = "phone";

    public static final String COL_PRODUCER_ID = "id";
    public static final String COL_PRODUCER_NAME = "name";

    public static final String COL_PHONE_ID = COL_PRODUCER_ID;
    public static final String COL_PHONE_NAME = COL_PRODUCER_NAME;
    public static final String COL_PHONE_PRICE = "price";
    public static final String COL_PHONE_PRODUCER_ID = "pro_id";

    public static final String SQL_CREATE_TABLE_PRODUCER =
            "CREATE TABLE " + TABLE_PRODUCER + "(" +
                COL_PRODUCER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL_PRODUCER_NAME + " TEXT" +
            ")";

    public static final String SQL_CREATE_TABLE_PHONE =
            "CREATE TABLE " + TABLE_PHONE + "(" +
                    COL_PHONE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_PHONE_NAME + " TEXT, " +
                    COL_PHONE_PRICE + " FLOAT, " +
                    COL_PHONE_PRODUCER_ID + " INTEGER" +
            ")";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_PRODUCER);
        db.execSQL(SQL_CREATE_TABLE_PHONE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
