package com.example.mac.phonenumber;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import com.example.mac.phonenumber.dao.PhoneDao;
import com.example.mac.phonenumber.dao.PhoneDaoImp;
import com.example.mac.phonenumber.dialog.PhoneDialog;
import com.example.mac.phonenumber.dialog.BrandDialog;
import com.example.mac.phonenumber.model.Phone;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, PhoneListener {
    private ListView mListView;
    private PhoneAdapter mAdapter;

    private PhoneDao mPhoneDao;

    private List<Phone> mPhones;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final DbHelper dbHelper = new DbHelper(this);
        mPhoneDao = new PhoneDaoImp(dbHelper);
        mPhones = mPhoneDao.getAllPhones();

        findViewById(R.id.addProducerButton).setOnClickListener(this);
        findViewById(R.id.addPhoneButton).setOnClickListener(this);

        mAdapter = new PhoneAdapter(mPhones, this);

        mListView = findViewById(R.id.phoneListView);
        mListView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mPhones.clear();
        mPhones.addAll(mPhoneDao.getAllPhones());

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPhoneAdded(Phone phone) {
        mPhones.add(phone);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPhoneRemove(Phone phone) {
        mPhones.remove(phone);
        mPhoneDao.removePhone(phone);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPhoneClick(Phone phone) {
        new PhoneDialog(this, phone).show();
    }

    @Override
    public void onPhoneUpdated(Phone phone) {
        mPhones.set(mPhones.indexOf(phone), phone);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addProducerButton:
                new BrandDialog(this).show();
                break;

            case R.id.addPhoneButton:
                new PhoneDialog(this).show();
                break;
        }
    }
}
