package com.example.mac.phonenumber;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.mac.phonenumber.model.Phone;

import java.util.List;

public class PhoneAdapter extends BaseAdapter {
    private List<Phone> mPhones;
    private PhoneListener mListener;

    public PhoneAdapter(List<Phone> phones, PhoneListener listener) {
        mPhones = phones;
        mListener = listener;
    }

    @Override
    public int getCount() {
        return mPhones.size();
    }

    @Override
    public Object getItem(int position) {
        return mPhones.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mPhones.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_phone, parent, false);
            holder = new ViewHolder(convertView, position);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Phone phone = mPhones.get(position);
        holder.bind(phone);

        return convertView;
    }

    class ViewHolder implements View.OnClickListener {
        private int mPosition;

        private TextView mNameTextView;
        private TextView mPriceTextView;
        private TextView mProducerTextView;
        private ImageButton mRemoveButton;

        public ViewHolder(View view, int position) {
            mPosition = position;

            mNameTextView = view.findViewById(R.id.nameTextView);
            mPriceTextView = view.findViewById(R.id.priceTextView);
            mProducerTextView = view.findViewById(R.id.producerTextView);
            mRemoveButton = view.findViewById(R.id.removeButton);

            mRemoveButton.setOnClickListener(this);
            view.setOnClickListener(this);
        }

        private void bind(final Phone phone) {
            mNameTextView.setText(phone.getName());
            mPriceTextView.setText(String.valueOf(phone.getPrice()));
            mProducerTextView.setText(phone.getBrand().getName());
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.removeButton:
                    mListener.onPhoneRemove(mPhones.get(mPosition));
                    break;

                default:
                    mListener.onPhoneClick(mPhones.get(mPosition));
            }
        }
    }
}
