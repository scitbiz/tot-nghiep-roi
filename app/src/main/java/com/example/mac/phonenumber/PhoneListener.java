package com.example.mac.phonenumber;

import com.example.mac.phonenumber.model.Phone;

public interface PhoneListener {
    void onPhoneAdded(Phone phone);

    void onPhoneRemove(Phone phone);

    void onPhoneClick(Phone phone);

    void onPhoneUpdated(Phone phone);
}
