package com.example.mac.phonenumber.dao;

import com.example.mac.phonenumber.model.Brand;

import java.util.List;

public interface BrandDao {
    List<Brand> getAllProducers();

    Brand getProducer(int id);

    long addProducer(Brand brand);

    boolean updateProducer(Brand brand);

    Brand removeProducer(Brand brand);
}
