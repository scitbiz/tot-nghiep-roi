package com.example.mac.phonenumber.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.mac.phonenumber.DbHelper;
import com.example.mac.phonenumber.model.Brand;

import java.util.ArrayList;
import java.util.List;

import static com.example.mac.phonenumber.DbHelper.*;

public class BrandDaoImp implements BrandDao {
    private DbHelper mDbHelper;

    public BrandDaoImp(DbHelper dbHelper) {
        mDbHelper = dbHelper;
    }

    @Override
    public List<Brand> getAllProducers() {
        final SQLiteDatabase db = mDbHelper.getReadableDatabase();
        final Cursor cursor = db.query(TABLE_PRODUCER, null, null, null, null, null, null);

        final List<Brand> brands = new ArrayList<>();

        if (cursor != null) {
            while (cursor.moveToNext()) {
                final int id = cursor.getInt(0);
                final String name = cursor.getString(1);

                brands.add(new Brand(id, name));
            }

            cursor.close();
            db.close();
        }

        return brands;
    }

    @Override
    public Brand getProducer(int id) {
        final SQLiteDatabase db = mDbHelper.getReadableDatabase();

        final String where = COL_PRODUCER_ID + " = ?";
        final String[] whereArgs = new String[]{ String.valueOf(id) };
        final Cursor cursor = db.query(TABLE_PRODUCER, null, where, whereArgs, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();

            final String name = cursor.getString(1);

            final Brand brand = new Brand(id, name);

            cursor.close();
            db.close();

            return brand;
        }

        return null;
    }

    @Override
    public long addProducer(Brand brand) {
        final SQLiteDatabase db = mDbHelper.getWritableDatabase();

        final ContentValues values = new ContentValues();
//        values.put(COL_PRODUCER_ID, brand.getId());
        values.put(COL_PRODUCER_NAME, brand.getName());

        return db.insert(TABLE_PRODUCER, null, values);
    }

    @Override
    public boolean updateProducer(Brand brand) {
        final SQLiteDatabase db = mDbHelper.getWritableDatabase();

        final String where = COL_PRODUCER_ID + " = ?";
        final String[] whereArgs = new String[] {
            String.valueOf(brand.getId())
        };
        final int rowChanges = db.update(TABLE_PRODUCER, brand.toContentValues(), where, whereArgs);

        return rowChanges > 0;
    }

    @Override
    public Brand removeProducer(Brand brand) {
        final SQLiteDatabase db = mDbHelper.getWritableDatabase();

        final String where = COL_PRODUCER_ID + " = ?";
        final String[] whereArgs = new String[] {
            String.valueOf(brand.getId())
        };

        final int rowChanges = db.delete(TABLE_PRODUCER, where, whereArgs);
        return rowChanges > 0 ? brand : null;
    }
}
