package com.example.mac.phonenumber.dao;

import com.example.mac.phonenumber.model.Phone;

import java.util.List;

public interface PhoneDao {
    List<Phone> getAllPhones();
    
    Phone getPhone(int id);

    long addPhone(Phone Phone);
    
    boolean updatePhone(Phone Phone);
    
    Phone removePhone(Phone Phone);
}
