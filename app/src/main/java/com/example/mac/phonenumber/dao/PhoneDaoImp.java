package com.example.mac.phonenumber.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.mac.phonenumber.DbHelper;
import com.example.mac.phonenumber.model.Phone;
import com.example.mac.phonenumber.model.Brand;

import java.util.ArrayList;
import java.util.List;

import static com.example.mac.phonenumber.DbHelper.*;

public class PhoneDaoImp implements PhoneDao {
    private DbHelper mDbHelper;

    public PhoneDaoImp(DbHelper dbHelper) {
        mDbHelper = dbHelper;
    }
    
    public Phone cursorToPhone(Cursor cursor) {
        final int id = cursor.getInt(0);
        final String name = cursor.getString(1);
        final double price = cursor.getDouble(2);
        final int phoneId = cursor.getInt(3);
        final Brand phone = new BrandDaoImp(mDbHelper).getProducer(phoneId);
        
        return new Phone(id, name, price, phone);
    }

    @Override
    public List<Phone> getAllPhones() {
        final SQLiteDatabase db = mDbHelper.getReadableDatabase();
        final Cursor cursor = db.query(TABLE_PHONE, null, null, null, null, null, null);

        final List<Phone> phones = new ArrayList<>();

        if (cursor != null) {
            while (cursor.moveToNext()) {
                final Phone phone = cursorToPhone(cursor);
                
                phones.add(phone);
            }

            cursor.close();
            db.close();
        }

        return phones;
    }

    @Override
    public Phone getPhone(int id) {
        final SQLiteDatabase db = mDbHelper.getReadableDatabase();

        final String tableNames = TABLE_PHONE + "," + TABLE_PRODUCER;
        final String where = TABLE_PHONE + "." + COL_PHONE_ID + " = ?";
        final String[] whereArgs = new String[]{ String.valueOf(id) };
        final Cursor cursor = db.query(tableNames , null, where, whereArgs, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
            
            final Phone phone = cursorToPhone(cursor);

            cursor.close();
            db.close();

            return phone;
        }

        return null;
    }

    @Override
    public long addPhone(Phone phone) {
        final SQLiteDatabase db = mDbHelper.getWritableDatabase();

        final ContentValues values = new ContentValues();
//        values.put(COL_PHONE_ID, phone.getId());
        values.put(COL_PHONE_NAME, phone.getName());
        values.put(COL_PHONE_PRICE, phone.getPrice());
        values.put(COL_PHONE_PRODUCER_ID, phone.getBrand().getId());

        return db.insert(TABLE_PHONE, null, values);
    }

    @Override
    public boolean updatePhone(Phone phone) {
        final SQLiteDatabase db = mDbHelper.getWritableDatabase();

        final String where = COL_PHONE_ID + " = ?";
        final String[] whereArgs = new String[] {
                String.valueOf(phone.getId())
        };
        final int rowChanges = db.update(TABLE_PHONE, phone.toContentValues(), where, whereArgs);

        return rowChanges > 0;
    }

    @Override
    public Phone removePhone(Phone phone) {
        final SQLiteDatabase db = mDbHelper.getWritableDatabase();

        final String where = COL_PHONE_ID + " = ?";
        final String[] whereArgs = new String[] {
                String.valueOf(phone.getId())
        };

        final int rowChanges = db.delete(TABLE_PHONE, where, whereArgs);
        return rowChanges > 0 ? phone : null;
    }
}
