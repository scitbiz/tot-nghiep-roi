package com.example.mac.phonenumber.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.mac.phonenumber.DbHelper;
import com.example.mac.phonenumber.R;
import com.example.mac.phonenumber.Utils;
import com.example.mac.phonenumber.dao.BrandDao;
import com.example.mac.phonenumber.dao.BrandDaoImp;
import com.example.mac.phonenumber.model.Brand;

public class BrandDialog extends Dialog implements View.OnClickListener {
    private Button mAddButton;
    private EditText mNameEditText;
    private BrandDao mBrandDao;

    public BrandDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_producer);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        DbHelper dbHelper = new DbHelper(getContext());
        mBrandDao = new BrandDaoImp(dbHelper);

        mAddButton = findViewById(R.id.addButton);
        mAddButton.setOnClickListener(this);

        mNameEditText = findViewById(R.id.nameEditText);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addButton:
                final String name = mNameEditText.getText().toString().trim();
                if (name.isEmpty() || name.length() <= 10) {
                    Utils.showMessageDialog(getContext(), "Lỗi", "Tên quá ngắn!");
                    return;
                }

                mBrandDao.addProducer(new Brand(name));

                dismiss();
                break;
        }
    }
}
