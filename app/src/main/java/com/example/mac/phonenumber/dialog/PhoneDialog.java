package com.example.mac.phonenumber.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.mac.phonenumber.DbHelper;
import com.example.mac.phonenumber.PhoneListener;
import com.example.mac.phonenumber.R;
import com.example.mac.phonenumber.Utils;
import com.example.mac.phonenumber.dao.PhoneDao;
import com.example.mac.phonenumber.dao.PhoneDaoImp;
import com.example.mac.phonenumber.dao.BrandDao;
import com.example.mac.phonenumber.dao.BrandDaoImp;
import com.example.mac.phonenumber.model.Brand;
import com.example.mac.phonenumber.model.Phone;

import java.util.ArrayList;
import java.util.List;

public class PhoneDialog extends Dialog implements View.OnClickListener {
    private Button mAddButton;
    private EditText mNameEditText;
    private EditText mPriceEditText;
    private Spinner mBrandSpinner;

    private BrandDao mBrandDao;
    private PhoneDao mPhoneDao;

    private boolean isEditing = false;
    private Phone mPhone;
    private List<Brand> mBrands;

    private PhoneListener mListener;

    public PhoneDialog(Context context) {
        super(context);

        mListener = (PhoneListener) context;
    }

    public PhoneDialog(Context context, Phone phone) {
        super(context);

        mListener = (PhoneListener) context;
        mPhone = phone;
        isEditing = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_phone);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        DbHelper dbHelper = new DbHelper(getContext());
        mBrandDao = new BrandDaoImp(dbHelper);
        mPhoneDao = new PhoneDaoImp(dbHelper);

        mBrands = mBrandDao.getAllProducers();

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, getProducerNames());

        mNameEditText = findViewById(R.id.nameEditText);
        mPriceEditText = findViewById(R.id.priceEditText);

        mBrandSpinner = findViewById(R.id.producerSpinner);
        mBrandSpinner.setAdapter(adapter);

        mAddButton = findViewById(R.id.addButton);
        mAddButton.setOnClickListener(this);

        if (mPhone != null) {
            mNameEditText.setText(mPhone.getName());
            mPriceEditText.setText(String.valueOf(mPhone.getPrice()));
            mBrandSpinner.setSelection(mBrands.indexOf(mPhone.getBrand()));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addButton:
                final String name = mNameEditText.getText().toString().trim();
                final String price = mPriceEditText.getText().toString().trim();
                final int producerPos = mBrandSpinner.getSelectedItemPosition();
                final Brand brand = mBrands.get(producerPos);

                if (name.isEmpty() || name.length() <= 10) {
                    Utils.showMessageDialog(getContext(), "Lỗi", "Tên quá ngắn!");
                    return;
                }

                if (price.isEmpty()) {
                    Utils.showMessageDialog(getContext(), "Lỗi", "Giá không được để trống");
                    return;
                }

                if (isEditing) {
                    mPhone.setName(name);
                    mPhone.setPrice(Double.parseDouble(price));
                    mPhone.setBrand(brand);

                    mListener.onPhoneUpdated(mPhone);
                } else {
                    mPhone = new Phone(name, Double.parseDouble(price), brand);

                    int id = (int) mPhoneDao.addPhone(mPhone);
                    mPhone.setId(id);

                    mListener.onPhoneAdded(mPhone);
                }
                dismiss();
                break;
        }
    }

    private List<String> getProducerNames() {
        List<String> producerNames = new ArrayList<>();

        for (Brand brand : mBrands) {
            producerNames.add(brand.getName());
        }

        return producerNames;
    }
}
