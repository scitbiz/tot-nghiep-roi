package com.example.mac.phonenumber.model;

import android.content.ContentValues;
import android.os.Bundle;

import static com.example.mac.phonenumber.DbHelper.*;

public class Brand {
    private int mId;
    private String mName;

    public Brand(String name) {
        mId = 0;
        mName = name;
    }

    public Brand(int id, String name) {
        mId = id;
        mName = name;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public ContentValues toContentValues() {
        final ContentValues values = new ContentValues();
        values.put(COL_PRODUCER_ID, mId);
        values.put(COL_PRODUCER_NAME, mName);

        return values;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Brand) {
            return ((Brand) obj).getId() == mId;
        }

        return false;
    }
}
