package com.example.mac.phonenumber.model;

import android.content.ContentValues;

import static com.example.mac.phonenumber.DbHelper.*;

public class Phone {
    private int mId;
    private String mName;
    private double mPrice;
    private Brand mBrand;

    public Phone(String name, double price, Brand brand) {
        mId = 0;
        mName = name;
        mPrice = price;
        mBrand = brand;
    }

    public Phone(int id, String name, double price, Brand brand) {
        mId = id;
        mName = name;
        mPrice = price;
        mBrand = brand;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public double getPrice() {
        return mPrice;
    }

    public void setPrice(double price) {
        mPrice = price;
    }

    public Brand getBrand() {
        return mBrand;
    }

    public void setBrand(Brand brand) {
        mBrand = brand;
    }

    public ContentValues toContentValues() {
        final ContentValues values = new ContentValues();
        values.put(COL_PHONE_ID, mId);
        values.put(COL_PHONE_NAME, mName);
        values.put(COL_PHONE_PRICE, mPrice);
        values.put(COL_PHONE_PRODUCER_ID, mBrand.getId());

        return values;
    }
}
